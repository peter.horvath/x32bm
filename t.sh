#!/bin/bash
arch="$1"
typ="$2"

[ "$arch" = "" ] && arch=32
[ "$typ" = "" ] && typ=dbg

var_n=32
cmd_n=10000
iter_n=100000

ops="+-*^"

echo "#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {"

for ((a=0; a<$var_n; a++))
do
  echo "  uint64_t v$a=rand();"
done

echo -n "
  void dump() {
    printf (\""
for ((a=0; a<$var_n; a++))
do
  echo -n "v$a=%llu"
  if [ "$a" -lt $[var_n-1] ]
  then
    echo -n " "
  else
    echo -n "\\n"
  fi
done
echo -n '"'
for ((a=0; a<$var_n; a++))
do
  echo -n ",(long long unsigned int)v$a";
done
echo ");
  }

  dump();"

echo "  for (int a=0; a<$iter_n; a++) {";

for ((a=0; a<$cmd_n; a++))
do
  while true
  do
    var1="v$[RANDOM%$var_n]"
    var2="v$[RANDOM%$var_n]"
    var3="v$[RANDOM%$var_n]"
    if [ "$var1" != "$var2" ] && [ "$var1" != "$var3" ] && [ "$var2" != "$var3" ]
    then
      break
    fi
  done
  cmd="$var1^=$var2${ops:$[RANDOM%${#ops}]:1}$var3"
  [ "$typ" = "dbg" ] && echo "    printf(\"$cmd\\n\");"
  echo "    $cmd;"
  [ "$typ" = "dbg" ] && [ "$a" -lt $[cmd_n-1] ] && echo "    dump();"
done

echo "  }"
echo "  dump();"
echo "  return 0;
}"
