#!/usr/bin/make -f
ARCHS = 32 x32 64
TYPS = dbg rel
EXTS = ".c" ".s" ""

COMPILE_rel = -O3
COMPILE_dbg = -O0 -g
LINK_rel = 
LINK_dbg = -g

.PHONY: default
default: all

define CHAIN

t_$(1)_$(2).c: t.sh
	./t.sh $(1) $(2) >t_$(1)_$(2).c

t_$(1)_$(2).s: t_$(1)_$(2).c
	gcc -S -m$(1) -o t_$(1)_$(2).s $(COMPILE_$(2)) -Wall t_$(1)_$(2).c

t_$(1)_$(2): t_$(1)_$(2).s
	gcc -m$(1) $(LINK_$(2)) -o t_$(1)_$(2) t_$(1)_$(2).s -Wall

endef

$(foreach arch,$(ARCHS), $(foreach typ,$(TYPS), $(eval $(call CHAIN,$(arch),$(typ))) ) )

.PHONY: all
all: $(foreach arch,$(ARCHS), $(foreach typ,$(TYPS), t_$(arch)_$(typ) )  )

.PHONY: test
test: all
	./test.sh

.PHONY: clean
clean:
	rm -vf $(foreach arch,$(ARCHS), $(foreach typ,$(TYPS), $(foreach ext, $(EXTS), t_$(arch)_$(typ)$(ext) ) )  )
