# x32bm

## Getting started

- `make -j` - generates the test sources and then compiles them
- `./test.sh` - executes the tests twice, and then it prints the stat of the second run
- `./t.sh <dbg|rel> <32|x32|64>` - script to generate a specific source
